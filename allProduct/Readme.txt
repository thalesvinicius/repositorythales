Readme - Product by Thales Vinícius

Objective:
	This document explains how the allProducto application was created and also contains the explanations for running it.

The App:
	The application was created using Spring Boot, in order to facilitate application server configurations, because the Srping Boot adds several production grade services to application with little effort.
	The application also used Hibernat to persist the information in an H2 database.

What you’ll need
	URL Source: https://thalesvinicius@bitbucket.org/thalesvinicius/repositorythales.git

	The services available through the allProducts application can be called from any application / client that enables GET / POST requests.
	Example: Postman, SoapUI, etc.

Running the application
	1- Go to path application
	2- mvn spring-boot:run

Running the Tests
	1- Go to path application
	2- mvn spring-boot:run

DataBase:
	The file /allProduct/src/main/resources/import.sql is responsible for insert new products in H2 DataBase.
	The aplication was create to run this file and create the datas.

The problem:
	We have a Product Entity with One to Many relationship with Image entity

	Product also has a Many to One relationship with itself (Many Products to one Parent Product)

	Build a Restful service using JAX-RS to perform CRUD operations on a Product resource using Image as a sub-resource of Product.

	Your API classes should also perform these operations:

	a) Get all products excluding relationships (child products, images)
	b) Get all products including specified relationships (child product and/or images)
	c) Same as 1 using specific product identity
	d) Same as 2 using specific product identity
	e) Get set of child products for specific product
	f) Get set of images for specific product

The Solution:
	a) Get all products excluding relationships (child products, images)
		Request example (GET): http://localhost:8080/product/all
	b) Get all products including specified relationships (child product and/or images)
		Request example (GET): http://localhost:8080/product/allIncludingRelationships
	c) Same as 1 using specific product identity
		Request example (GET): http://localhost:8080/product/byName/Mouse or http://localhost:8080/product/byId/1
	d) Same as 2 using specific product identity
		Request example (GET): http://localhost:8080/product/includingRelationshipsById/1
	e) Get set of child products for specific product
		Request example (GET): http://localhost:8080/product/childProductsById/1
	f) Get set of images for specific product
		Request example (GET): http://localhost:8080/product/image/byProductId/6

	To add a product:
		Request example (POST): http://localhost:8080/product (Param JSON of Product)
			Example JSON:
				{
					"name": "Product Teste"
				}

	To add a image:
		Request example (POST): http://localhost:8080/product/image (Param JSON of Image)
			Example JSON:
				{
					"type": "image Teste"
				}

Note:
I was unable to finalize the exception handling as I would like, since LOG is still displaying the StackTrace error.
Since it's the first time I'm working with Spring Boot, I'd need a little more time.
I did not do a DTO for service return, but it's an improvement for the application.

Used Libraries:
	junit\junit\4.11\junit-4.11.jar
	org\hamcrest\hamcrest-core\1.3\hamcrest-core-1.3.jar
	com\h2database\h2\1.4.192\h2-1.4.192.jar
	org\springframework\boot\spring-boot-starter-web\1.3.1.RELEASE\spring-boot-starter-web-1.3.1.RELEASE.jar
	org\springframework\boot\spring-boot-starter\1.3.1.RELEASE\spring-boot-starter-1.3.1.RELEASE.jar
	org\springframework\boot\spring-boot\1.3.1.RELEASE\spring-boot-1.3.1.RELEASE.jar
	org\springframework\boot\spring-boot-autoconfigure\1.3.1.RELEASE\spring-boot-autoconfigure-1.3.1.RELEASE.jar
	org\springframework\boot\spring-boot-starter-logging\1.3.1.RELEASE\spring-boot-starter-logging-1.3.1.RELEASE.jar
	ch\qos\logback\logback-classic\1.1.3\logback-classic-1.1.3.jar
	ch\qos\logback\logback-core\1.1.3\logback-core-1.1.3.jar
	org\slf4j\jul-to-slf4j\1.7.13\jul-to-slf4j-1.7.13.jar
	org\slf4j\log4j-over-slf4j\1.7.13\log4j-over-slf4j-1.7.13.jar
	org\yaml\snakeyaml\1.16\snakeyaml-1.16.jar
	org\springframework\boot\spring-boot-starter-tomcat\1.3.1.RELEASE\spring-boot-starter-tomcat-1.3.1.RELEASE.jar
	org\apache\tomcat\embed\tomcat-embed-core\8.0.30\tomcat-embed-core-8.0.30.jar
	org\apache\tomcat\embed\tomcat-embed-el\8.0.30\tomcat-embed-el-8.0.30.jar
	org\apache\tomcat\embed\tomcat-embed-logging-juli\8.0.30\tomcat-embed-logging-juli-8.0.30.jar
	org\apache\tomcat\embed\tomcat-embed-websocket\8.0.30\tomcat-embed-websocket-8.0.30.jar
	org\springframework\boot\spring-boot-starter-validation\1.3.1.RELEASE\spring-boot-starter-validation-1.3.1.RELEASE.jar
	org\hibernate\hibernate-validator\5.2.2.Final\hibernate-validator-5.2.2.Final.jar
	javax\validation\validation-api\1.1.0.Final\validation-api-1.1.0.Final.jar
	com\fasterxml\classmate\1.1.0\classmate-1.1.0.jar
	com\fasterxml\jackson\core\jackson-databind\2.6.4\jackson-databind-2.6.4.jar
	com\fasterxml\jackson\core\jackson-annotations\2.6.4\jackson-annotations-2.6.4.jar
	com\fasterxml\jackson\core\jackson-core\2.6.4\jackson-core-2.6.4.jar
	org\springframework\spring-web\4.2.4.RELEASE\spring-web-4.2.4.RELEASE.jar
	org\springframework\spring-aop\4.2.4.RELEASE\spring-aop-4.2.4.RELEASE.jar
	aopalliance\aopalliance\1.0\aopalliance-1.0.jar
	org\springframework\spring-beans\4.2.4.RELEASE\spring-beans-4.2.4.RELEASE.jar
	org\springframework\spring-context\4.2.4.RELEASE\spring-context-4.2.4.RELEASE.jar
	org\springframework\spring-webmvc\4.2.4.RELEASE\spring-webmvc-4.2.4.RELEASE.jar
	org\springframework\spring-expression\4.2.4.RELEASE\spring-expression-4.2.4.RELEASE.jar
	org\springframework\boot\spring-boot-starter-data-jpa\1.3.1.RELEASE\spring-boot-starter-data-jpa-1.3.1.RELEASE.jar
	org\springframework\boot\spring-boot-starter-aop\1.3.1.RELEASE\spring-boot-starter-aop-1.3.1.RELEASE.jar
	org\aspectj\aspectjweaver\1.8.7\aspectjweaver-1.8.7.jar
	org\springframework\boot\spring-boot-starter-jdbc\1.3.1.RELEASE\spring-boot-starter-jdbc-1.3.1.RELEASE.jar
	org\apache\tomcat\tomcat-jdbc\8.0.30\tomcat-jdbc-8.0.30.jar
	org\apache\tomcat\tomcat-juli\8.0.30\tomcat-juli-8.0.30.jar
	org\springframework\spring-jdbc\4.2.4.RELEASE\spring-jdbc-4.2.4.RELEASE.jar
	org\hibernate\hibernate-entitymanager\4.3.11.Final\hibernate-entitymanager-4.3.11.Final.jar
	org\jboss\logging\jboss-logging\3.3.0.Final\jboss-logging-3.3.0.Final.jar
	org\jboss\logging\jboss-logging-annotations\1.2.0.Beta1\jboss-logging-annotations-1.2.0.Beta1.jar
	org\hibernate\hibernate-core\4.3.11.Final\hibernate-core-4.3.11.Final.jar
	antlr\antlr\2.7.7\antlr-2.7.7.jar
	org\jboss\jandex\1.1.0.Final\jandex-1.1.0.Final.jar
	dom4j\dom4j\1.6.1\dom4j-1.6.1.jar
	xml-apis\xml-apis\1.0.b2\xml-apis-1.0.b2.jar
	org\hibernate\common\hibernate-commons-annotations\4.0.5.Final\hibernate-commons-annotations-4.0.5.Final.jar
	org\hibernate\javax\persistence\hibernate-jpa-2.1-api\1.0.0.Final\hibernate-jpa-2.1-api-1.0.0.Final.jar
	org\javassist\javassist\3.18.1-GA\javassist-3.18.1-GA.jar
	javax\transaction\javax.transaction-api\1.2\javax.transaction-api-1.2.jar
	org\springframework\data\spring-data-jpa\1.9.2.RELEASE\spring-data-jpa-1.9.2.RELEASE.jar
	org\springframework\data\spring-data-commons\1.11.2.RELEASE\spring-data-commons-1.11.2.RELEASE.jar
	org\springframework\spring-orm\4.2.4.RELEASE\spring-orm-4.2.4.RELEASE.jar
	org\springframework\spring-tx\4.2.4.RELEASE\spring-tx-4.2.4.RELEASE.jar
	org\slf4j\slf4j-api\1.7.13\slf4j-api-1.7.13.jar
	org\slf4j\jcl-over-slf4j\1.7.13\jcl-over-slf4j-1.7.13.jar
	org\springframework\spring-aspects\4.2.4.RELEASE\spring-aspects-4.2.4.RELEASE.jar
	org\springframework\boot\spring-boot-starter-thymeleaf\1.3.1.RELEASE\spring-boot-starter-thymeleaf-1.3.1.RELEASE.jar
	org\thymeleaf\thymeleaf-spring4\2.1.4.RELEASE\thymeleaf-spring4-2.1.4.RELEASE.jar
	org\thymeleaf\thymeleaf\2.1.4.RELEASE\thymeleaf-2.1.4.RELEASE.jar
	ognl\ognl\3.0.8\ognl-3.0.8.jar
	org\unbescape\unbescape\1.1.0.RELEASE\unbescape-1.1.0.RELEASE.jar
	nz\net\ultraq\thymeleaf\thymeleaf-layout-dialect\1.3.1\thymeleaf-layout-dialect-1.3.1.jar
	org\codehaus\groovy\groovy\2.4.4\groovy-2.4.4.jar
	org\springframework\boot\spring-boot-starter-test\1.3.1.RELEASE\spring-boot-starter-test-1.3.1.RELEASE.jar
	org\mockito\mockito-core\1.10.19\mockito-core-1.10.19.jar
	org\objenesis\objenesis\2.1\objenesis-2.1.jar
	org\hamcrest\hamcrest-library\1.3\hamcrest-library-1.3.jar
	org\springframework\spring-core\4.2.4.RELEASE\spring-core-4.2.4.RELEASE.jar
	org\springframework\spring-test\4.2.4.RELEASE\spring-test-4.2.4.RELEASE.jar
