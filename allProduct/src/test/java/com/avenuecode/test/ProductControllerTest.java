package com.avenuecode.test;

import static org.assertj.core.api.BDDAssertions.then;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.avenuecode.exceptions.InvalidDataProductException;
import com.avenuecode.exceptions.ProductNotExistException;
import com.avenuecode.main.Application;
import com.avenuecode.model.Product;

/**
 * Integration test of ProductController to run the application.
 * 
 * @author thales.vinicius
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(properties = { "management.port=0" })
public class ProductControllerTest {

	@LocalServerPort
	private int port;

	@Autowired
	private TestRestTemplate testRestTemplate;

	/**
	 * Test create a product with valid name sending request to controller.
	 * 
	 * @throws Exception
	 */
	@Test
	public void shouldCreateProductValidNameSendingRequestToController()
			throws Exception {
		ResponseEntity<Product> entity = this.testRestTemplate.postForEntity(
				"http://localhost:" + this.port + "/product/", new Product(
						"Product test"), Product.class);

		then(entity.getStatusCode()).isEqualTo(HttpStatus.FOUND);
	}

	/**
	 * Test create a product with invalid name sending request to controller
	 * 
	 * @throws Exception
	 */
	@Test
	public void shouldCreateProductInvalidNameSendingRequestToController()
			throws InvalidDataProductException {
		ResponseEntity<Product> entity = this.testRestTemplate.postForEntity(
				"http://localhost:" + this.port + "/product/", new Product(""),
				Product.class);

		then(entity.getStatusCode())
				.isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);
	}

	/**
	 * Test delete a product exist sending request to controller.
	 * 
	 * @throws Exception
	 */
	@Test
	public void shouldDeleteProductExistSendingRequestToController()
			throws Exception {
		ResponseEntity<Product> entity = this.testRestTemplate.postForEntity(
				"http://localhost:" + this.port + "/product/delete/7",
				new Product(), Product.class);

		then(entity.getStatusCode()).isEqualTo(HttpStatus.FOUND);
	}

	/**
	 * Test delete product not exist sending request to controller.
	 * 
	 * @throws Exception
	 */
	@Test
	public void shouldDeleteProductNotExistSendingRequestToController()
			throws ProductNotExistException {
		ResponseEntity<Product> entity = this.testRestTemplate.postForEntity(
				"http://localhost:" + this.port + "/product/delete/60",
				new Product(), Product.class);

		then(entity.getStatusCode())
				.isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);
	}

	/**
	 * Test return a product when sending request to controller.
	 * 
	 * @throws Exception
	 */
	@Test
	public void shouldReturnProductWhenSendingRequestToController()
			throws Exception {
		ResponseEntity<Product> entity = this.testRestTemplate.getForEntity(
				"http://localhost:" + this.port + "/product/byId/1",
				Product.class);

		then(entity.getStatusCode()).isEqualTo(HttpStatus.OK);
	}

	/**
	 * Test return all products when sending request to controller.
	 * 
	 * @throws Exception
	 */
	@Test
	public void shouldReturnAllProductWhenSendingRequestToController()
			throws Exception {
		Product[] entity = this.testRestTemplate.getForObject(
				"http://localhost:" + this.port + "/product/all",
				Product[].class);

		then(entity != null && entity.length > 0);
	}

	/**
	 * Test return all products irncluding relationships when sending request to
	 * controller.
	 * 
	 * @throws Exception
	 */
	@Test
	public void shouldReturnAllIncludingRelationshipsProductWhenSendingRequestToController()
			throws Exception {
		Object[] entity = this.testRestTemplate.getForObject(
				"http://localhost:" + this.port
						+ "/product/allIncludingRelationships", Object[].class);

		then(entity != null && entity.length > 0);
	}

	/**
	 * Test return products by name when sending request to controller.
	 * 
	 * @throws Exception
	 */
	@Test
	public void shouldReturnProductByNameWhenSendingRequestToController()
			throws Exception {
		Product[] entity = this.testRestTemplate.getForObject(
				"http://localhost:" + this.port + "/product/byName/Mouse",
				Product[].class);

		then(entity != null && entity.length > 0);
	}

	/**
	 * Test return products including your relationships by id when sending
	 * request to controller.
	 * 
	 * @throws Exception
	 */
	@Test
	public void shouldReturnProductIncludingRelationshipsByIdWhenSendingRequestToController()
			throws Exception {
		Object[] entity = this.testRestTemplate.getForObject(
				"http://localhost:" + this.port
						+ "/product/includingRelationshipsById/1",
				Object[].class);

		then(entity != null && entity.length > 0);
	}

	/**
	 * Test return childs products by id when sending request to controller.
	 * 
	 * @throws Exception
	 */
	@Test
	public void shouldReturnChildProductsByIdWhenSendingRequestToController()
			throws Exception {
		Object[] entity = this.testRestTemplate.getForObject(
				"http://localhost:" + this.port
						+ "/product/childProductsById/1", Object[].class);

		then(entity != null && entity.length > 0);
	}

}
