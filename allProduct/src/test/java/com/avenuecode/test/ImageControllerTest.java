package com.avenuecode.test;

import static org.assertj.core.api.BDDAssertions.then;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.avenuecode.exceptions.InvalidDataImageException;
import com.avenuecode.main.Application;
import com.avenuecode.model.Image;

/**
 * Integration test ImageController to run the application.
 * 
 * @author thales.vinicius
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(properties = { "management.port=0" })
public class ImageControllerTest {

	@LocalServerPort
	private int port;

	@Autowired
	private TestRestTemplate testRestTemplate;

	/**
	 * Test return image by product id when sending request to controller.
	 * 
	 * @throws Exception
	 */
	@Test
	public void shouldReturnImageByProductIdWhenSendingRequestToController()
			throws Exception {
		ResponseEntity<Image> entity = this.testRestTemplate.getForEntity(
				"http://localhost:" + this.port + "/product/image/byId/1",
				Image.class);

		then(entity.getStatusCode()).isEqualTo(HttpStatus.OK);
	}

	/**
	 * Test return images by product Id when sending request to controller.
	 * 
	 * @throws Exception
	 */
	@Test
	public void shouldReturnImagesByProductIdWhenSendingRequestToController()
			throws Exception {
		Image[] entity = this.testRestTemplate.getForObject("http://localhost:"
				+ this.port + "/product/image/byProductId/1", Image[].class);

		then(entity != null && entity.length > 0);
	}

	/**
	 * Test create a image with valid type sending request to controller.
	 * 
	 * @throws Exception
	 */
	@Test
	public void shouldCreateImageValidTypeSendingRequestToController()
			throws Exception {
		ResponseEntity<Image> entity = this.testRestTemplate.postForEntity(
				"http://localhost:" + this.port + "/product/image/", new Image(
						"Image test"), Image.class);

		then(entity.getStatusCode()).isEqualTo(HttpStatus.FOUND);
	}

	/**
	 * Test create a image with invalid type sending request to controller.
	 * 
	 * @throws Exception
	 */
	@Test
	public void shouldCreateImageInvalidTypeSendingRequestToController()
			throws InvalidDataImageException {
		ResponseEntity<Image> entity = this.testRestTemplate.postForEntity(
				"http://localhost:" + this.port + "/product/image/", new Image(
						""), Image.class);

		then(entity.getStatusCode())
				.isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);
	}
}
