package com.avenuecode.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.avenuecode.model.Product;

/**
 * Interface for methods related to product rules.
 * @author thales.vinicius
 * @since 10/12/2016
 *
 */
public interface ProductRepository extends JpaRepository<Product, Long> {

	/**
	 * Find a product from its name. Returns a list because there may be more
	 * than one product with the same name.
	 * 
	 * @param name
	 * @return list of products
	 */
	List<Product> findByName(String name);
	
	/**
	 * Find product by Id
	 * 
	 * @param id
	 * @return Product
	 */
	Product findById(Long id);
	
	/**
	 * Find all products. Returns a list.
	 * 
	 * @return list of products
	 */
	List<Product> findAll();	
	
	/**
	 * Get all products including specified relationships (child product and/or images)
	 * 
	 * @return list of Products.
	 */
	@Query("SELECT p, i, parent FROM Image i left join i.product p left join p.parent parent")
	public List<Product> findProductsIncludingRelationships();

	/**
	 * 
	 * @param id
	 * @return
	 */
	@Query("SELECT p, i, parent FROM Image i left join i.product p left join p.parent parent Where p.id = :id")
	public List<Product> findProductsIncludingRelationshipsById(@Param("id") Long id);
	
	/**
	 * 
	 * @param id
	 * @return
	 */
	@Query("SELECT p FROM Product p join p.parent parent Where parent.id = :id")
	public List<Product> findChildProductsById(@Param("id") Long id);
	
}
