package com.avenuecode.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.avenuecode.model.Image;

/**
 * Interface related to image rules.
 * 
 * @author thales.vinicius
 * @since 10/12/2016
 *
 */
public interface ImageRepository extends JpaRepository<Image, Long> {

	/**
	 * Get set of images for specific product 
	 * 
	 * @param id Image
	 * @return list of images.
	 */
	@Query("Select i From Image i join i.product product Where product.id = :id")
	public List<Image> findImagesByProductId(@Param("id") Long id);
	
	/**
	 * Find image by Id
	 * 
	 * @param id of image
	 * @return Image
	 */
	Image findById(Long id);

}
