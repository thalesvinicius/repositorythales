package com.avenuecode.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.avenuecode.exceptions.ImageNotExistException;
import com.avenuecode.exceptions.InvalidDataImageException;
import com.avenuecode.exceptions.InvalidDataProductException;
import com.avenuecode.exceptions.ProductNotExistException;
import com.avenuecode.model.Image;
import com.avenuecode.repository.ImageRepository;

/**
 * Class controller for business rules related to the image entity.
 * 
 * @author thales.vinicius
 * @since 10/12/2016
 */
@Controller
@RequestMapping("/product/image")
public class ImageController {

	private ImageRepository imageRepository;
	
	@Autowired
	public ImageController(ImageRepository imageRepository) {
		this.imageRepository = imageRepository;
	}
    
    /**
     * Get image by specific image identity.
     * 
     * @param id for search
     * @return Information of image.
     */
	@RequestMapping(value = "/byId/{id}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody Image findProductById(@PathVariable("id") Long id) {
		return imageRepository.findById(id);
	}
	
	/**
	 * Get set of images for specific product 
	 * 
	 * @param id
	 * @return List of Images
	 */
	@RequestMapping(value = "/byProductId/{id}", method = RequestMethod.GET, produces="application/json")
    public @ResponseBody List<Image> findChildProductsById(@PathVariable("id") Long id) {
        List<Image> listImages = imageRepository.findImagesByProductId(id);
        return listImages;
    }
    
    @RequestMapping(method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    public String addImage(@RequestBody Image image) throws InvalidDataImageException {

        if (image.getType() == null || image.getType() != null && "".equals(image.getType().trim())) {
            throw new InvalidDataImageException("Invalid type Image.");
        }

        imageRepository.save(image);
        return "redirect:/product/image/byId/" + image.getId();
    }
    
    /**
     * Delete a image by ID.
     * 
     * @param id of image
     * @throws InvalidDataProductException
     * @throws ProductNotExistException 
     */
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE, produces="application/json")
    public String deleteImagebyId(@PathVariable("id") Long id) throws InvalidDataImageException, ImageNotExistException {
    	if (id == null) {
    		throw new InvalidDataImageException("Invalid id image.");
    	}
    	
    	Image image = imageRepository.findById(id);
    	
    	if (image == null) {
    		throw new ImageNotExistException("Image not exist.");
    	}
    	
    	imageRepository.delete(image);
    	return "redirect:/product/image/byId/" + image.getId();
    }    
	
}
