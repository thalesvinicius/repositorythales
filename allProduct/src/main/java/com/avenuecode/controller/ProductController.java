package com.avenuecode.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.avenuecode.exceptions.InvalidDataProductException;
import com.avenuecode.exceptions.ProductNotExistException;
import com.avenuecode.model.Product;
import com.avenuecode.repository.ProductRepository;

/**
 * Class controller for business rules related to the product entity.
 * 
 * @author thales.vinicius
 * @since 10/12/2016
 */
@Controller
@RequestMapping("/product")
public class ProductController {
	
	private ProductRepository productRepository;
	
	public ProductController() {
		
	}
	
	@Autowired
	public ProductController(ProductRepository productRepository) {
		this.productRepository = productRepository;
	}
	
	/**
	 * Get all products excluding relationships (child products, images)
	 * 
	 * @return list of products
	 */
    @RequestMapping(value = "/all", method = RequestMethod.GET, produces="application/json")
    public @ResponseBody List<Product> listAllProducts() {
          return productRepository.findAll();
    }
    
    /**
     * Get all products including specified relationships (child product and/or images)
     *  
     * @return List of Products
     */
    //TODO - create ProductResponse to return.
    @RequestMapping(value = "/allIncludingRelationships", method = RequestMethod.GET, produces="application/json")
    public @ResponseBody List<Product> findProductsIncludingRelationships() {
          return productRepository.findProductsIncludingRelationships();
    }
	
	/**
	 * Find a product from its name. Returns a list because there may be more
	 * than one product with the same name.
	 * 
	 * @param name - Name for search
	 * @return Information of product.
	 */
    @RequestMapping(value = "/byName/{name}", method = RequestMethod.GET, produces="application/json")
    public @ResponseBody List<Product> findProductsByName(@PathVariable("name") String name) {
          return productRepository.findByName(name);
    }
    
    /**
     * Get product by specific product identity.
     * 
     * @param id for search
     * @return Information of product.
     */
    @RequestMapping(value = "/byId/{id}", method = RequestMethod.GET, produces = "application/json")
    public
    @ResponseBody
    Product findProductById(@PathVariable("id") Long id) {
        return productRepository.findById(id);
    }    
	
    /**
     * Get all products including specified relationships by product identity.
     *  
     * @param id
     * @return Product
     */
    @RequestMapping(value = "/includingRelationshipsById/{id}", method = RequestMethod.GET, produces="application/json")
    public @ResponseBody List<Product> findProductsIncludingRelationshipsById(@PathVariable("id") Long id) {
        return productRepository.findProductsIncludingRelationshipsById(id);
    }
    
    /**
     * Get set of child products for specific product
     * 
     * @param id
     * @return List of products
     */
    @RequestMapping(value = "/childProductsById/{id}", method = RequestMethod.GET, produces="application/json")
    public @ResponseBody List<Product> findChildProductsById(@PathVariable("id") Long id) {
        List<Product> listProducts = productRepository.findChildProductsById(id);
        return listProducts;
    }
    
    /**
     * Add new Product
     * 
     * @param name
     * @param product
     * @return
     * @throws InvalidDataProductException 
     */
    @RequestMapping(method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    public String addProduct(@RequestBody Product product) throws InvalidDataProductException {

        if (product.getName() == null || product.getName() != null && "".equals(product.getName().trim())) {
            throw new InvalidDataProductException("Invalid name product.");
        }

        productRepository.save(product);
        return "redirect:/product/byId/" + product.getId();
    }    
    
    /**
     * Delete a product by ID.
     * 
     * @param id
     * @throws InvalidDataProductException
     * @throws ProductNotExistException 
     */
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE, produces="application/json")
    public String deleteProductbyId(@PathVariable("id") Long id) throws InvalidDataProductException, ProductNotExistException {
    	if (id == null) {
    		throw new InvalidDataProductException("Invalid id product.");
    	}
    	
    	Product product = productRepository.findById(id);
    	
    	if (product == null) {
    		throw new ProductNotExistException("Product not exist.");
    	}
    	
    	productRepository.delete(product);
    	return "redirect:/product/byId/{id}";
    }
  
}
