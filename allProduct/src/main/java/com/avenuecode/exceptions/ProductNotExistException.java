package com.avenuecode.exceptions;

/**
 * Class to exceptions about not exist product.
 * 
 * @author thales.vinicius
 *
 */
public class ProductNotExistException extends ApplicationException {

	private static final long serialVersionUID = -6571341457133668890L;

	/**
	 * Constructs an ProductNotExistException object with the message passed
	 * by parameter
	 * 
	 * @param msg
	 */
	public ProductNotExistException(String msg) {
		super(msg);
	}

	/**
	 * Constructs a ProductNotExistException object with message and the
	 * cause of this exception, used to chain exceptions
	 * 
	 * @param msg
	 * @param cause
	 */
	public ProductNotExistException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
