package com.avenuecode.exceptions;

/**
 * Class to exceptions about invalid data products.
 * 
 * @author thales.vinicius
 *
 */
public class InvalidDataProductException extends ApplicationException {

	private static final long serialVersionUID = 1149241039409861914L;

	/**
	 * Constructs an InvalidDataProductException object with the message passed
	 * by parameter
	 * 
	 * @param msg
	 */
	public InvalidDataProductException(String msg) {
		super(msg);
	}

	/**
	 * Constructs a InvalidDataProductException object with message and the
	 * cause of this exception, used to chain exceptions
	 * 
	 * @param msg
	 * @param cause
	 */
	public InvalidDataProductException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
