package com.avenuecode.exceptions;

/**
 * Class to exceptions about not exist product.
 * 
 * @author thales.vinicius
 *
 */
public class ImageNotExistException extends ApplicationException {

	private static final long serialVersionUID = 1149241039409861914L;

	/**
	 * Constructs an ImageNotExistException object with the message passed
	 * by parameter
	 * 
	 * @param msg
	 */
	public ImageNotExistException(String msg) {
		super(msg);
	}

	/**
	 * Constructs a ImageNotExistException object with message and the
	 * cause of this exception, used to chain exceptions
	 * 
	 * @param msg
	 * @param cause
	 */
	public ImageNotExistException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
