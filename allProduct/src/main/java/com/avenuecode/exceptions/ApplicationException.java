package com.avenuecode.exceptions;

public class ApplicationException extends Exception {

	private static final long serialVersionUID = 8080879473670607562L;

	/**
	 * Constructs an ApplicationException object with the message passed
	 * by parameter
	 * 
	 * @param msg
	 */
	public ApplicationException(String msg) {
		super(msg);
	}

	/**
	 * Constructs a ApplicationException object with message and the
	 * cause of this exception, used to chain exceptions
	 * 
	 * @param msg
	 * @param cause
	 */
	public ApplicationException(String msg, Throwable cause) {
		super(msg, cause);
	}

}
