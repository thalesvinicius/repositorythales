package com.avenuecode.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EntityScan(basePackages = { "com.avenuecode.model" })
@EnableJpaRepositories(basePackages = { "com.avenuecode.repository" })
@ComponentScan(basePackages = {"com.avenuecode.controller"})
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);		
	}

}
