package com.avenuecode.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * Class to represent entity image
 * 
 * @author thales.vinicius
 * @since 10/12/2016
 *
 */
@Entity
public class Image {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO) 
	private Long id;
    
    @Column(name="type", nullable=false)
	private String type;
    
    @ManyToOne
    @JoinColumn(name="product_id")
    private Product product;

    public Image(String type) {
    	this.type = type;
    }
    
    public Image() {

    }
    
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Product getProduct_id() {
		return product;
	}

	public void setProduct_id(Product product_id) {
		this.product = product_id;
	}
	
	@Override
    public String toString() {
        return String.format(
                "Image[id=%d, type='%s']",
                id, type);
    }
	
}
