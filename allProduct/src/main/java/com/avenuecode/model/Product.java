package com.avenuecode.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Class to represent entity product
 * 
 * @author thales.vinicius
 * @since 10/12/2016
 *
 */
@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Product {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
    
    @NotNull
    @Column(name="name", nullable=false)
    private String name;

    @Column(name="description", nullable=true)
    private String Description;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "parent_product_id", insertable = true, updatable = true, nullable = true)
    private Product parent;
    
    public Product(String name) {
		this.name = name;
	}    
    
    public Product(String name, Product parent_product_id) {
		this.name = name;
		this.parent = parent_product_id;
	}

    public Product() {

    }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
    
    public String getDescription() {
		return Description;
	}

	public void setDescription(String description) {
		Description = description;
	}

	public Product getParent_product_id() {
		return parent;
	}

	public void setParent_product_id(Product parent_product_id) {
		this.parent = parent_product_id;
	}

	@Override
    public String toString() {
        return String.format(
                "Product[id=%d, name='%s', parent_product_id='%s']",
                id, name, parent);
    }
	
}
