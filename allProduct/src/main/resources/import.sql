--Insert the products
insert into product(name, description) values ('Computer', 'Computer Dell - i7');
insert into product(name, description, parent_product_id) values ('Keyboard', 'Keyboard Dell - PTBR', 1);
insert into product(name, description, parent_product_id) values ('Mouse', 'Mouse Dell', 1);
insert into product(name, description) values ('Ball', 'Ball for tennis');
insert into product(name, description) values ('Table', 'Table black');
insert into product(name, description) values ('Bicycle', 'Bicycle 21 speeds');

--Insert the images
insert into image(type, product_id) values ('Computing', 1);
insert into image(type, product_id) values ('Computing', 2);
insert into image(type, product_id) values ('Computing', 3);
insert into image(type, product_id) values ('Toy', 4);
insert into image(type, product_id) values ('Mobile', 5);
insert into image(type, product_id) values ('Sports', 6);
insert into image(type, product_id) values ('Sports 2', 6);
insert into image(type, product_id) values ('Sports 3', 6);
